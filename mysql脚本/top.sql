

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `blog`
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'JFinal Demo Title here', 'JFinal Demo Content here');
INSERT INTO `blog` VALUES ('2', 'test 1', 'test 1');
INSERT INTO `blog` VALUES ('3', 'test 2', 'test 2');
INSERT INTO `blog` VALUES ('4', 'test 3', 'test 3');
INSERT INTO `blog` VALUES ('5', 'test 4', 'test 4');



-- ----------------------------
-- Table structure for `CmsCategory`
-- ----------------------------
DROP TABLE IF EXISTS `CmsCategory`;
CREATE TABLE `CmsCategory` (
  `id` varchar(50) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `shortname` varchar(100) DEFAULT NULL,
  `code` varchar(400) DEFAULT NULL,
  `customcode` varchar(200) DEFAULT NULL,
  `parentid` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `disable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of CmsCategory
-- ----------------------------
INSERT INTO `CmsCategory` VALUES ('5067fa75-396a-44ad-8b76-6c43d5412005', '打打网球', '但我却', '0004', '32131', null, '0', '0');
INSERT INTO `CmsCategory` VALUES ('2ddf7f38-b48f-43fc-8574-2798f0b3cf13', '额为', '321 ', '0003', '313 3', null, '0', '0');

-- ----------------------------
-- Table structure for `CmsCateNews`
-- ----------------------------
DROP TABLE IF EXISTS `CmsCateNews`;
CREATE TABLE `CmsCateNews` (
  `id` varchar(50) NOT NULL,
  `cateid` varchar(50) DEFAULT NULL,
  `newsid` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `delete` tinyint(4) DEFAULT '0',
  `disable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of CmsCateNews
-- ----------------------------

-- ----------------------------
-- Table structure for `CmsNews`
-- ----------------------------
DROP TABLE IF EXISTS `CmsNews`;
CREATE TABLE `CmsNews` (
  `id` varchar(50) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `userid` varchar(50) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `alertuserid` varchar(50) DEFAULT NULL,
  `alerttime` datetime DEFAULT NULL,
  `delete` tinyint(4) DEFAULT '0',
  `disable` tinyint(4) DEFAULT '0',
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of CmsNews
-- ----------------------------

-- ----------------------------
-- Table structure for `FrameAuth`
-- ----------------------------
DROP TABLE IF EXISTS `FrameAuth`;
CREATE TABLE `FrameAuth` (
  `id` varchar(50) NOT NULL,
  `resource` varchar(50) DEFAULT NULL,
  `resourceType` varchar(50) DEFAULT NULL,
  `grantTo` varchar(50) DEFAULT NULL,
  `grantToType` varchar(50) DEFAULT NULL,
  `openAll` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `FrameAuth_ID` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameAuth
-- ----------------------------
INSERT INTO `FrameAuth` VALUES ('f8e9399e-0873-4dc4-a73c-679fde6f5409', '4ca217e5-9298-4e11-9278-c0cbff57648a', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('f84ddaed-600e-4c06-b72a-b9078171f29c', 'b6757f12-d377-4eb6-ac3f-40a6bb633d16', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('5ffc116f-c07f-44e6-8f7d-24aaf0d6a0a4', '75adde17-65db-4723-aaa1-183a437ce4c4', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('0e59daa9-03a9-4806-881c-968138a50db0', '54122d8c-d211-4cf1-af79-f837d5b9a8e6', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('897f5186-3916-4c70-9a96-b1c9b96bb4e3', '11356518-9735-4aff-87e2-46c4c5d58e67', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('061f75cb-f6ce-4f7a-8394-6fde127468ea', '4dac8838-3818-4023-99a4-f98553f7006d', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('998b9f2b-854b-46a0-8760-ee7c613be49e', 'f5a6563a-f87d-43e2-90b0-1e00de189d5c', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('e14cfa0f-7e5b-4bed-bb02-71b5fcdd7902', '2b186277-8835-4fa1-a09a-a06f2092dd17', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('6f8dc0fa-45b9-41f3-89c8-9754bcf34d65', 'f1f6bb15-f86e-456e-8a97-b5f71c931e70', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('3f6c3ba8-5bc9-41cb-95ff-c5238740b1e8', '07282d80-44ef-4908-9d48-b09f96f7d2b8', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('7366e36a-40a9-4e24-8394-a89d71f46d93', '5a4b3c14-1f66-4731-bf3d-191b9417a62a', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('667626fc-97f0-482d-ac1c-a7f14a296d5a', 'a58127db-bc93-4068-a16f-7e45b040579a', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('33c1fa81-3195-408f-8e96-77b4b26724ca', '472b40b6-83ed-4d6f-8e1b-65f6d2bcd952', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('b214ea09-f2d5-4eff-9e26-e90c63748532', 'a0acbdbf-714c-4746-883a-a7d489aa20b0', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('a998cabb-eb60-4763-be0f-244ee198fcb9', 'c5533edd-ece3-46d3-a59f-12d7fdc7e5a9', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('81548de7-0d27-47b4-91c0-2fe3b1daf719', 'f407a592-db14-46a7-bbac-c836fcca0833', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('78fb7314-8376-4635-9fd9-26e055cf714a', '1cc34918-593a-4384-9a72-8d96ac44c3dd', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('25f58e5d-1c75-4575-8646-506021afedf1', '703a9df4-7ac3-4815-ac20-8424e6deae24', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('c1f2f2c9-7ad4-4840-994f-63b88503b303', '998bcae9-6183-4bf1-a2c2-bf2c76e60a43', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('af79ad08-cc24-4b14-8cf3-24f75283ddd5', '659df59c-39a4-451a-bf99-2419dc682b10', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('b2fae171-6536-497d-a157-5077c4683dc9', 'de001af1-deb4-4223-b45a-04ae4c304bd6', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('82b5cd48-487a-4572-b735-854f9b464c10', '7b2f762d-9fca-4a99-a733-065dd7b76cc2', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('2ba8a5b8-ab88-4052-9b5e-aab10d9dfdd0', '5cbfd91f-50dc-4b46-8bb1-ce7b188ac63d', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('98c3e7f1-5c2a-40e4-b018-e4d4b0c6e343', 'eedbdd47-5d8f-4d1e-8ad0-9eec8ced7a14', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('db97ac80-c29d-4112-8a9b-50b05215bfc9', 'e7805c78-68cb-48e5-9480-4aacfa727f0b', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('d87c3189-6887-48c6-961d-89f34be5afb8', '1bda5e74-c786-4b70-be8b-912782d74e07', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('03f456e2-305d-4027-aee8-84cef1ef9b62', '5060def6-b349-4e5a-8e83-38d8518128e4', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('3ab1b51f-19af-4301-8ad5-e8d19d29a464', '4480aeab-e122-4bf1-b9cc-e20a56c51a29', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('69e01bcb-2420-4d60-8664-89b90a7432a1', 'd0fed651-c52d-459f-b2b8-71f688b52911', 'MENU', null, null, '1');
INSERT INTO `FrameAuth` VALUES ('6052aa44-273f-487e-b170-0ffc71a491bf', 'd374fb7d-fefa-4b26-9127-19515a02daf5', 'MENU', null, null, '1');

-- ----------------------------
-- Table structure for `FrameDept`
-- ----------------------------
DROP TABLE IF EXISTS `FrameDept`;
CREATE TABLE `FrameDept` (
  `id` varchar(50) NOT NULL,
  `deptName` varchar(50) DEFAULT NULL,
  `deptCode` varchar(50) DEFAULT NULL,
  `parentId` varchar(50) DEFAULT NULL,
  `isreal` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameDept
-- ----------------------------
INSERT INTO `FrameDept` VALUES ('39097b73-9af8-4d7c-b84d-8ee5f272cea8', 'rrrrrr', '0001', null, '1', '1');
INSERT INTO `FrameDept` VALUES ('c29466b3-974e-4382-af7d-78c857fcb55f', '开发一部', '00020001', '3887df09-698c-44b8-ab40-eaf238af76dd', '1', '1');

-- ----------------------------
-- Table structure for `FrameDeptDuty`
-- ----------------------------
DROP TABLE IF EXISTS `FrameDeptDuty`;
CREATE TABLE `FrameDeptDuty` (
  `id` varchar(50) NOT NULL,
  `deptId` varchar(50) DEFAULT NULL,
  `dutyName` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameDeptDuty
-- ----------------------------

-- ----------------------------
-- Table structure for `FrameMail`
-- ----------------------------
DROP TABLE IF EXISTS `FrameMail`;
CREATE TABLE `FrameMail` (
  `id` varchar(50) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `sendUser` varchar(50) DEFAULT NULL,
  `sendUserName` varchar(50) DEFAULT NULL,
  `sendTime` datetime DEFAULT NULL,
  `statue` int(11) DEFAULT '0',
  `mailboxid` varchar(50) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameMail
-- ----------------------------
INSERT INTO `FrameMail` VALUES ('53e876ff-361f-41bf-ad4e-8128f2ad46f3', '测试', '额范围额范围我额外', '1', 'admin', '2013-06-04 20:00:41', '1', '0001', '0');
INSERT INTO `FrameMail` VALUES ('1fb5c076-3e35-4746-a44a-afd725f83ec2', 'test email', 'hehe', '1', '系统管理员', '2013-07-01 11:04:32', '1', '0001', '0');
INSERT INTO `FrameMail` VALUES ('379406e2-ac71-44ba-8f0e-23656a645f45', 'test', 'test', '1', '系统管理员', '2013-09-02 19:44:18', '1', '0001', '0');
INSERT INTO `FrameMail` VALUES ('88f4abf7-7beb-4f56-a261-c4da5382b61d', '', '', '1', '系统管理员', '2013-09-03 09:22:14', '1', '0001', '0');
INSERT INTO `FrameMail` VALUES ('b6cd25e8-cbcb-4604-876d-08cdde115bff', 'test', 'sfsf', '1', '系统管理员', '2013-09-03 11:05:28', '1', '0001', '0');
INSERT INTO `FrameMail` VALUES ('819ae3fd-7f9c-48ac-a359-f9e19af87b23', 'gaoiyuah', 'fdsfdsfsdfdsfdsf', '1', '系统管理员', '2013-09-03 14:05:26', '1', '0001', '0');

-- ----------------------------
-- Table structure for `FrameMailUser`
-- ----------------------------
DROP TABLE IF EXISTS `FrameMailUser`;
CREATE TABLE `FrameMailUser` (
  `id` varchar(50) NOT NULL,
  `mailId` varchar(50) DEFAULT NULL,
  `belongUser` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '0',
  `hasread` tinyint(4) DEFAULT '0',
  `readtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameMailUser
-- ----------------------------
INSERT INTO `FrameMailUser` VALUES ('827457c7-bf6a-411a-ac94-66e827999bef', '53e876ff-361f-41bf-ad4e-8128f2ad46f3', '1', '1', '0', null);
INSERT INTO `FrameMailUser` VALUES ('9a185a7b-12a6-44a6-88a1-63021cf89093', '53e876ff-361f-41bf-ad4e-8128f2ad46f3', '22', '1', '0', null);
INSERT INTO `FrameMailUser` VALUES ('a85eee28-16f9-42f4-b7c1-6b6c259ea578', '1fb5c076-3e35-4746-a44a-afd725f83ec2', '22', '1', '0', null);

-- ----------------------------
-- Table structure for `FrameMenu`
-- ----------------------------
DROP TABLE IF EXISTS `FrameMenu`;
CREATE TABLE `FrameMenu` (
  `id` varchar(50) NOT NULL,
  `menuUrl` varchar(50) DEFAULT NULL,
  `menuName` varchar(50) DEFAULT NULL,
  `menuCode` varchar(50) DEFAULT NULL,
  `menuIcon` varchar(20) DEFAULT NULL,
  `parentId` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameMenu
-- ----------------------------
INSERT INTO `FrameMenu` VALUES ('472b40b6-83ed-4d6f-8e1b-65f6d2bcd952', '/framemenu/list', '模块管理', '000700010006', null, '5060def6-b349-4e5a-8e83-38d8518128e4', '90', 'MENU');
INSERT INTO `FrameMenu` VALUES ('5060def6-b349-4e5a-8e83-38d8518128e4', '@', '组织架构管理', '00070001', null, 'f1f6bb15-f86e-456e-8a97-b5f71c931e70', '0', 'MENU');
INSERT INTO `FrameMenu` VALUES ('e7805c78-68cb-48e5-9480-4aacfa727f0b', '相关Demo', '相关Demo', '0003', null, null, '2', 'MENU');
INSERT INTO `FrameMenu` VALUES ('a58127db-bc93-4068-a16f-7e45b040579a', '/upload', '文件上传', '00030002', null, 'e7805c78-68cb-48e5-9480-4aacfa727f0b', '99', 'MENU');
INSERT INTO `FrameMenu` VALUES ('de001af1-deb4-4223-b45a-04ae4c304bd6', '/frameuserconfig/index', '个人配置', '00080001', null, '1cc34918-593a-4384-9a72-8d96ac44c3dd', '-1', 'MENU');
INSERT INTO `FrameMenu` VALUES ('75adde17-65db-4723-aaa1-183a437ce4c4', '/frameuser/list', '用户管理', '000700010005', null, '5060def6-b349-4e5a-8e83-38d8518128e4', '80', 'MENU');
INSERT INTO `FrameMenu` VALUES ('998bcae9-6183-4bf1-a2c2-bf2c76e60a43', '/framedept/list', '部门管理', '000700010009', null, '5060def6-b349-4e5a-8e83-38d8518128e4', '100', 'MENU');
INSERT INTO `FrameMenu` VALUES ('7b2f762d-9fca-4a99-a733-065dd7b76cc2', '#', '代办事项', '00080002', null, '1cc34918-593a-4384-9a72-8d96ac44c3dd', '2', 'MENU');
INSERT INTO `FrameMenu` VALUES ('d374fb7d-fefa-4b26-9127-19515a02daf5', '/demo/testFck', '富文本框', '00030003', null, 'e7805c78-68cb-48e5-9480-4aacfa727f0b', '3333', 'MENU');
INSERT INTO `FrameMenu` VALUES ('11356518-9735-4aff-87e2-46c4c5d58e67', '/frameroletype/list', '角色类别管理', '0007000100110001', null, '4dac8838-3818-4023-99a4-f98553f7006d', '70', 'MENU');
INSERT INTO `FrameMenu` VALUES ('b6757f12-d377-4eb6-ac3f-40a6bb633d16', '/demo/msg', '个性化弹出框', '00030004', null, 'e7805c78-68cb-48e5-9480-4aacfa727f0b', '88', 'MENU');
INSERT INTO `FrameMenu` VALUES ('eedbdd47-5d8f-4d1e-8ad0-9eec8ced7a14', '/demo/calendar', '日程控件', '00030005', null, 'e7805c78-68cb-48e5-9480-4aacfa727f0b', '3', 'MENU');
INSERT INTO `FrameMenu` VALUES ('5a4b3c14-1f66-4731-bf3d-191b9417a62a', '#', '我的邮箱', '0004', null, null, '200', 'MENU');
INSERT INTO `FrameMenu` VALUES ('2b186277-8835-4fa1-a09a-a06f2092dd17', '/framemail/inbox', '收件箱', '000400040002', null, 'f5a6563a-f87d-43e2-90b0-1e00de189d5c', '22', 'MENU');
INSERT INTO `FrameMenu` VALUES ('07282d80-44ef-4908-9d48-b09f96f7d2b8', '#', '发件箱', '000400040001', null, 'f5a6563a-f87d-43e2-90b0-1e00de189d5c', '222', 'MENU');
INSERT INTO `FrameMenu` VALUES ('4ca217e5-9298-4e11-9278-c0cbff57648a', '#', '回收箱', '000400040003', null, 'f5a6563a-f87d-43e2-90b0-1e00de189d5c', '11', 'MENU');
INSERT INTO `FrameMenu` VALUES ('f5a6563a-f87d-43e2-90b0-1e00de189d5c', '@', '内部邮箱', '00040004', null, '5a4b3c14-1f66-4731-bf3d-191b9417a62a', '44', 'MENU');
INSERT INTO `FrameMenu` VALUES ('1cc34918-593a-4384-9a72-8d96ac44c3dd', '个人事务', '个人事务', '0008', null, null, '4', 'MENU');
INSERT INTO `FrameMenu` VALUES ('4dac8838-3818-4023-99a4-f98553f7006d', '/framerole/list', '角色管理', '000700010011', null, '5060def6-b349-4e5a-8e83-38d8518128e4', '60', 'MENU');
INSERT INTO `FrameMenu` VALUES ('703a9df4-7ac3-4815-ac20-8424e6deae24', '/demo/button', '权限按钮', '00030006', null, 'e7805c78-68cb-48e5-9480-4aacfa727f0b', '1', 'MENU');
INSERT INTO `FrameMenu` VALUES ('1bda5e74-c786-4b70-be8b-912782d74e07', '/frameduty/index', '部门职务管理', '0007000100090001', null, '998bcae9-6183-4bf1-a2c2-bf2c76e60a43', '1', 'MENU');
INSERT INTO `FrameMenu` VALUES ('54122d8c-d211-4cf1-af79-f837d5b9a8e6', '/userrolerelation/list', '用户角色关系', '0007000100050001', null, '75adde17-65db-4723-aaa1-183a437ce4c4', '75', 'MENU');
INSERT INTO `FrameMenu` VALUES ('f1f6bb15-f86e-456e-8a97-b5f71c931e70', '@', '后台管理', '0007', null, null, '1', 'MENU');
INSERT INTO `FrameMenu` VALUES ('5cbfd91f-50dc-4b46-8bb1-ce7b188ac63d', '/frameauthorize/index', '模块授权', '0007000100060001', null, '472b40b6-83ed-4d6f-8e1b-65f6d2bcd952', '3', 'MENU');
INSERT INTO `FrameMenu` VALUES ('659df59c-39a4-451a-bf99-2419dc682b10', '/framemail/newMail', '写邮件', '000400040004', 'icon-mail', 'f5a6563a-f87d-43e2-90b0-1e00de189d5c', '100', 'MENU');
INSERT INTO `FrameMenu` VALUES ('a0acbdbf-714c-4746-883a-a7d489aa20b0', '@', '系统配置', '00070002', null, 'f1f6bb15-f86e-456e-8a97-b5f71c931e70', '2', 'MENU');
INSERT INTO `FrameMenu` VALUES ('c5533edd-ece3-46d3-a59f-12d7fdc7e5a9', '/frameportal/config', '首页元件配置', '000700020001', null, 'a0acbdbf-714c-4746-883a-a7d489aa20b0', '7', 'MENU');
INSERT INTO `FrameMenu` VALUES ('4480aeab-e122-4bf1-b9cc-e20a56c51a29', '@', '内容管理', '0010', null, null, '2', 'MENU');
INSERT INTO `FrameMenu` VALUES ('d0fed651-c52d-459f-b2b8-71f688b52911', '/framecms/categorymanager', '栏目管理', '00100001', null, '4480aeab-e122-4bf1-b9cc-e20a56c51a29', '0', 'MENU');
INSERT INTO `FrameMenu` VALUES ('f407a592-db14-46a7-bbac-c836fcca0833', '/framecms/newslist', '内容管理', '00100002', null, '4480aeab-e122-4bf1-b9cc-e20a56c51a29', '4', 'MENU');

-- ----------------------------
-- Table structure for `FrameMessage`
-- ----------------------------
DROP TABLE IF EXISTS `FrameMessage`;
CREATE TABLE `FrameMessage` (
  `id` varchar(50) NOT NULL,
  `content` varchar(100) DEFAULT NULL,
  `targetUrl` varchar(200) DEFAULT NULL,
  `tag` varchar(50) DEFAULT NULL,
  `tartgetId` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `belongto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameMessage
-- ----------------------------

-- ----------------------------
-- Table structure for `FramePortal`
-- ----------------------------
DROP TABLE IF EXISTS `FramePortal`;
CREATE TABLE `FramePortal` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `col` int(11) DEFAULT NULL,
  `row` int(11) DEFAULT NULL,
  `href` varchar(200) DEFAULT NULL,
  `enable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FramePortal
-- ----------------------------

-- ----------------------------
-- Table structure for `FrameRole`
-- ----------------------------
DROP TABLE IF EXISTS `FrameRole`;
CREATE TABLE `FrameRole` (
  `id` varchar(50) NOT NULL,
  `roleName` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `isReserved` int(11) DEFAULT NULL,
  `belongOuGuid` varchar(50) DEFAULT NULL,
  `roleTypeName` varchar(50) DEFAULT NULL,
  `roleTypeId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameRole
-- ----------------------------
INSERT INTO `FrameRole` VALUES ('5318ef25-82b3-4071-8c68-7e2494b93db2', '系统督办角色', '100', null, null, '项目管理', 'dadd665b-6077-431c-8f08-d506c1979356');
INSERT INTO `FrameRole` VALUES ('abc11286-5961-4be2-b83a-4f4b9fdd71b9', '公文收发员角色', null, null, null, '公文管理', 'bd463875-6904-49d1-91c5-af2fad62a2a9');
INSERT INTO `FrameRole` VALUES ('22391b9b-e7fb-43bc-9702-107db427c995', '测试', '1', null, null, '项目管理', 'dadd665b-6077-431c-8f08-d506c1979356');
INSERT INTO `FrameRole` VALUES ('2cd9f6c9-48bf-4d5f-bf81-8df02802ef38', '测试2', '1', null, null, '项目管理', 'dadd665b-6077-431c-8f08-d506c1979356');
INSERT INTO `FrameRole` VALUES ('2ffccd5e-39dd-47f8-a3f4-d3a46df8572f', '部门管理员', '0', null, null, '系统管理员', '32b2e564-b7b4-48dd-a2d7-1e199c3495e5');
INSERT INTO `FrameRole` VALUES ('e3fdd833-6de1-4dde-95d7-d173a8940c7a', '会员', '0', null, null, '普通用户', 'fca1665b-6d7a-4a46-983e-88a5f4c71db9');

-- ----------------------------
-- Table structure for `FrameRoleType`
-- ----------------------------
DROP TABLE IF EXISTS `FrameRoleType`;
CREATE TABLE `FrameRoleType` (
  `id` varchar(50) NOT NULL,
  `roleTypeName` varchar(50) DEFAULT NULL,
  `parentRoleTypeId` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameRoleType
-- ----------------------------
INSERT INTO `FrameRoleType` VALUES ('bd463875-6904-49d1-91c5-af2fad62a2a9', '公文管理', null, '90');
INSERT INTO `FrameRoleType` VALUES ('dadd665b-6077-431c-8f08-d506c1979356', '项目管理', null, '90');
INSERT INTO `FrameRoleType` VALUES ('32b2e564-b7b4-48dd-a2d7-1e199c3495e5', '系统管理员', null, '80');
INSERT INTO `FrameRoleType` VALUES ('fca1665b-6d7a-4a46-983e-88a5f4c71db9', '普通用户', null, '70');
INSERT INTO `FrameRoleType` VALUES ('4f618a82-b9b9-42af-b81b-d75bc276a159', null, null, null);
INSERT INTO `FrameRoleType` VALUES ('73595b8e-d4f1-449d-b2b1-afead02aa8c8', null, null, null);
INSERT INTO `FrameRoleType` VALUES ('2e5e9813-560c-45f8-891e-a6f6787c562d', null, null, null);

-- ----------------------------
-- Table structure for `FrameUploadFile`
-- ----------------------------
DROP TABLE IF EXISTS `FrameUploadFile`;
CREATE TABLE `FrameUploadFile` (
  `id` varchar(50) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `filepath` varchar(200) DEFAULT NULL,
  `filetime` bigint(20) DEFAULT NULL,
  `filetype` varchar(50) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `tag` varchar(50) DEFAULT NULL,
  `targetid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameUploadFile
-- ----------------------------
INSERT INTO `FrameUploadFile` VALUES ('d2e6981c-f32b-49b8-be3d-9944032eb301', 'null', '/UploadFiles/', '0', null, '0', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('0a68e150-fe16-4cfd-addd-073d4f08523f', 'null', '/UploadFiles/', '0', null, '0', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('3bfed3fc-e708-426c-a01d-b2673b431b2f', 'null', '/UploadFiles/', '1368717219919', null, '2347', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('c91ad853-0902-4018-8c83-9518ae4eb468', 'test3.html', '/UploadFiles/', '1368717312495', null, '2347', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('5549d935-0c64-4e6d-8fe3-dc42ef2cf41d', 'xhConnect.txt', '/UploadFiles/', '1368781129544', null, '479', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('912306ed-5d62-430a-95ef-7443c172ecd0', '20060526115628298.png', '/UploadFiles/', '1369047287225', null, '5086', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('010f3ef9-54c7-46d4-9c44-63f84e8155e1', 'mianma.jpg', '/UploadFiles/', '1369226470347', null, '385760', null, 'b1cc6cf3-12b0-4a72-9ed4-4ca786f156ba');
INSERT INTO `FrameUploadFile` VALUES ('a78bcc7c-f448-4324-8112-e3fac23e9fac', '1.jpg', '/UploadFiles/', '1370321924043', null, '71361', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('02bb9d49-eff6-4a83-b1b6-cf134f3a1f7a', 'scale-month-small-day.png', '/UploadFiles/', '1370321941711', null, '3635', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('6cfa5bb0-d639-4f60-8fcb-222b225ff89c', '11.jpg', '/UploadFiles/', '1370321960804', null, '71361', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('43937c20-d987-4068-9832-d9dd9721bf6b', 'dahe.jpg', '/UploadFiles/', '1370321966263', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('ffb68665-4f0f-4ab2-8903-f86e0927cfc3', 'dahe1.jpg', '/UploadFiles/', '1370322023706', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('59e8d30c-3bbe-4106-be1b-d7202ea3a229', 'dahe2.jpg', '/UploadFiles/', '1370322031853', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('d2d06a7e-c464-43eb-b4e6-1628ef84fd45', 'long.jpg', '/UploadFiles/', '1370322032025', null, '1673121', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('67cc66c9-225d-44ed-bd8b-d1429338bf4b', 'dahe3.jpg', '/UploadFiles/', '1370322046694', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('ce5a2fda-b76f-4261-87d7-dea4e6e8eabc', 'long1.jpg', '/UploadFiles/', '1370322046872', null, '1673121', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('99d67c1b-529a-46a7-9376-4b5477576742', 'dahe4.jpg', '/UploadFiles/', '1370322550952', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('37b3a9a1-a8e7-40c2-b7b3-592aa5ff5b70', 'dahe5.jpg', '/UploadFiles/', '1370322566921', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('634112e5-2b08-4713-a09d-1716738a2a5a', 'dahe6.jpg', '/UploadFiles/', '1370322697889', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('4c472f5a-45ad-478c-ba3b-b3b13d46f930', 'dahe7.jpg', '/UploadFiles/', '1370322706180', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('7b2471ab-7ca8-4472-8bcb-e60a796f8b14', 'dahe8.jpg', '/UploadFiles/', '1370322774761', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('07495c1c-333d-4fb6-a43b-3dde1b2117e3', 'dahe9.jpg', '/UploadFiles/', '1370322937561', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('8ba13376-ec39-4684-abd3-93e1f13119c5', 'dahe10.jpg', '/UploadFiles/', '1370323099487', null, '96202', null, 'null');
INSERT INTO `FrameUploadFile` VALUES ('0ab29fdb-3406-4ed5-bd9e-bb2260de34b4', 'dahe11.jpg', '/UploadFiles/', '1370323293538', null, '96202', null, null);
INSERT INTO `FrameUploadFile` VALUES ('a26f5fb6-2e2f-4e2f-8401-a261820e383a', 'dahe12.jpg', '/UploadFiles/', '1370323451632', null, '96202', null, '75e68345-8804-4a50-9f05-59b38797a154');
INSERT INTO `FrameUploadFile` VALUES ('9099efbd-ad37-4f5b-a62b-5b6deaba988b', 'dahe13.jpg', '/UploadFiles/', '1370323480115', null, '96202', null, 'ecf03feb-0e23-4391-8707-d7364454951f');
INSERT INTO `FrameUploadFile` VALUES ('6be933b5-c385-44f4-9f2c-7a62ab9e2207', 'dahe14.jpg', '/UploadFiles/', '1370323657713', null, '96202', 'MAIL', '181e9c3b-5f6e-4368-b5a1-d19930baba2e');
INSERT INTO `FrameUploadFile` VALUES ('71306286-7724-4eab-bf8f-b53f2b222b6e', 'dahe15.jpg', '/UploadFiles/', '1370323893394', null, '96202', 'MAIL', 'dac330ee-89be-410e-96f0-cea5a4d8f857');
INSERT INTO `FrameUploadFile` VALUES ('5ef3655a-827b-4d99-899f-d24b8da05041', 'dahe16.jpg', '/UploadFiles/', '1370324120144', null, '96202', 'MAIL', '7e296f16-b8e2-4eef-887a-d23778ba8cb8');
INSERT INTO `FrameUploadFile` VALUES ('294ae0a1-53e9-4ef5-9f55-30827f9a7874', 'dahe17.jpg', '/UploadFiles/', '1370324154392', null, '96202', 'MAIL', '9e1bc2ac-3872-476d-a3cc-d83bb1fd8a15');
INSERT INTO `FrameUploadFile` VALUES ('9a81b358-6aa8-4eb1-b203-c20dfa0ca7cf', 'dahe18.jpg', '/UploadFiles/', '1370324233528', null, '96202', 'MAIL', '246583b8-2922-4d75-963e-d0a589704dee');
INSERT INTO `FrameUploadFile` VALUES ('3fef06a2-f82c-455b-85c6-9b2b7183d0b7', 'dahe19.jpg', '/UploadFiles/', '1370324329072', null, '96202', 'MAIL', '246583b8-2922-4d75-963e-d0a589704dee');
INSERT INTO `FrameUploadFile` VALUES ('a12534df-a9c6-4673-b636-afc71e8a1c21', 'dahe20.jpg', '/UploadFiles/', '1370324400304', null, '96202', 'MAIL', '246583b8-2922-4d75-963e-d0a589704dee');
INSERT INTO `FrameUploadFile` VALUES ('189a2f46-c3df-4c95-ad13-08583d5b9748', 'dahe21.jpg', '/UploadFiles/', '1370324506070', null, '96202', 'MAIL', '7886917a-d357-4a3c-a999-04d6cc889f6c');
INSERT INTO `FrameUploadFile` VALUES ('d6223b2a-b4af-469d-9a7b-f569c2c63186', 'dahe22.jpg', '/UploadFiles/', '1370324916768', null, '96202', 'MAIL', '2e80691e-5a00-4b95-9241-a209f076de1e');
INSERT INTO `FrameUploadFile` VALUES ('c44575ec-2018-40b0-9cdd-f732c7206053', 'dahe23.jpg', '/UploadFiles/', '1370324963409', null, '96202', 'MAIL', '7950e78d-360f-4c9b-8191-7518a474a59e');
INSERT INTO `FrameUploadFile` VALUES ('a468298a-9d0a-4888-89b1-5308dc71d7d8', 'dahe24.jpg', '/UploadFiles/', '1370325072501', null, '96202', 'MAIL', '85c58c4b-604a-44b4-b5ff-b05a6971f61e');
INSERT INTO `FrameUploadFile` VALUES ('3cf1d257-20b6-423c-b102-499118e6492b', 'index_07-fu.jpg', '/UploadFiles/', '1372647998695', null, '45393', 'MAIL', '395c947b-dd51-4a3a-ad21-e9fe08a24986');
INSERT INTO `FrameUploadFile` VALUES ('d1eb3acc-9def-4d64-be51-6790d5274683', '爱消除.zip', '/UploadFiles/', '1378129010000', null, '1289524', null, null);
INSERT INTO `FrameUploadFile` VALUES ('cfd580da-c1a4-422a-a905-37c072880055', 'us.txt', '/UploadFiles/', '1378133785000', null, '182', null, null);
INSERT INTO `FrameUploadFile` VALUES ('85e1c2a8-79d1-4657-8146-66c4d84df48f', 'todo.txt', '/UploadFiles/', '1378133785000', null, '1632', null, null);
INSERT INTO `FrameUploadFile` VALUES ('0dea59e2-0195-4c15-a94d-4e962537ea33', 'black.png', '/UploadFiles/', '1378169363000', null, '143', null, null);
INSERT INTO `FrameUploadFile` VALUES ('974ea088-a978-410d-a5a8-a26cf452f497', 'blue.png', '/UploadFiles/', '1378169371000', null, '360', null, null);
INSERT INTO `FrameUploadFile` VALUES ('5af5cd32-82a7-4012-97a4-b9937fdef63d', '默认角色配置.jpeg', '/UploadFiles/', '1378169393000', null, '123907', 'MAIL', 'caf536c1-4f83-466e-afb5-2432f51c3dfc');
INSERT INTO `FrameUploadFile` VALUES ('49af3f0f-86d8-4d6b-a8ec-cbed56056110', 'D1_Y{71N0F3XD2T21SVXUAR.gif', '/UploadFiles/', '1378170661000', null, '3457', null, null);
INSERT INTO `FrameUploadFile` VALUES ('12d04eaf-687f-4d59-a1dc-e58efff73a09', 'fifm.cn.exe', '/UploadFiles/', '1378170938000', null, '52736', null, null);
INSERT INTO `FrameUploadFile` VALUES ('6700ef37-18a2-4585-954c-e44bbb5ced42', 'Calendar3.rar', '/UploadFiles/', '1378171324000', null, '4218', 'MAIL', '88f4abf7-7beb-4f56-a261-c4da5382b61d');
INSERT INTO `FrameUploadFile` VALUES ('f26453e3-b667-4a3c-981f-93b14f7d9f79', 'Ymodem动态链接库调用说明.txt', '/UploadFiles/', '1378171472000', null, '1203', null, null);
INSERT INTO `FrameUploadFile` VALUES ('63f6ea45-af60-42f5-9961-59075aea4674', '测试报告20130827-通知-wlpro.docx', '/UploadFiles/', '1378171475000', null, '203470', null, null);
INSERT INTO `FrameUploadFile` VALUES ('3856edcf-73db-46ee-9291-78e1f6bb3fd0', '深入学习MongoDB.pdf', '/UploadFiles/', '1378171499000', null, '9490569', null, null);
INSERT INTO `FrameUploadFile` VALUES ('bf4f32da-b845-458f-9cb8-91753d0a6d81', 'secondarytile.png', '/UploadFiles/', '1378173222000', null, '2455', null, null);
INSERT INTO `FrameUploadFile` VALUES ('c91621a3-71ab-465e-ba35-108fe71fd343', 'arrow_left.png', '/UploadFiles/', '1378179032000', null, '10356', null, null);
INSERT INTO `FrameUploadFile` VALUES ('4e01cb57-e851-40b4-b01b-ae861f30a8c0', 'Jellyfish.jpg', '/UploadFiles/', '1378179168000', null, '775702', null, null);
INSERT INTO `FrameUploadFile` VALUES ('5159babc-c69a-437c-b060-8d2c21392a12', 'Koala.jpg', '/UploadFiles/', '1378179168000', null, '780831', null, null);
INSERT INTO `FrameUploadFile` VALUES ('d7997ca9-0434-4ca7-be33-211a1342b835', 'Lighthouse.jpg', '/UploadFiles/', '1378179169000', null, '561276', null, null);
INSERT INTO `FrameUploadFile` VALUES ('46e9d927-3b92-4c9b-b4ed-4946cbf05195', 'QQProtect.exe', '/UploadFiles/', '1378179702000', null, '106168', null, null);
INSERT INTO `FrameUploadFile` VALUES ('9887967a-f18a-433e-8c2d-f5cc2d8e03d6', '飞秋FeiQ.exe', '/UploadFiles/', '1378188361000', null, '3305472', null, null);

-- ----------------------------
-- Table structure for `FrameUser`
-- ----------------------------
DROP TABLE IF EXISTS `FrameUser`;
CREATE TABLE `FrameUser` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `displayname` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `FrameUser_ID` (`id`) USING BTREE,
  UNIQUE KEY `FrameUser_UserName` (`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameUser
-- ----------------------------
INSERT INTO `FrameUser` VALUES ('1', 'admin', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '0', 'admin@126.com', '199999992222', '系统管理员', null, '0');
INSERT INTO `FrameUser` VALUES ('8', '123', '1A9B9508B6003B68DDFE03A9C8CBC4BD4388339B', '0', '13', '123', '123', null, '0');
INSERT INTO `FrameUser` VALUES ('20', 'jkgeng', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '1', null, null, 'BOSS', null, '0');
INSERT INTO `FrameUser` VALUES ('22', 'lolo', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '女', 'luoli@lo.lo', '109090909', '萝莉', null, '0');
INSERT INTO `FrameUser` VALUES ('cf771e90-5294-4787-a2bd-a4bb8f0277ef', 'test', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '男', '112', '111112', '测试人员', null, '0');
INSERT INTO `FrameUser` VALUES ('a71c10e8-2e73-403c-b036-7c299af60449', 'k1', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', 'nv', '11@12.13', '1234', 'k11', null, '0');
INSERT INTO `FrameUser` VALUES ('6050a0c8-b789-4d24-8514-aeef9b269fa6', 'f112', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '11112', '1112', '11112', '1112', null, '0');
INSERT INTO `FrameUser` VALUES ('addaab67-96ef-44b3-b040-81233742b774', 'ff1', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', 'ff1', 'ff1', 'ff1', 'ff1', null, '0');
INSERT INTO `FrameUser` VALUES ('12d968fc-7cbc-44e9-af9b-86a2ad29e959', 'cece', '7B21848AC9AF35BE0DDB2D6B9FC3851934DB8420', '1', '23', '32322', '测测', '2013-05-27', '0');

-- ----------------------------
-- Table structure for `FrameUserExt`
-- ----------------------------
DROP TABLE IF EXISTS `FrameUserExt`;
CREATE TABLE `FrameUserExt` (
  `id` varchar(50) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `theme` varchar(50) DEFAULT NULL,
  `sysMsgPosition` varchar(20) DEFAULT NULL,
  `tableSize` int(11) DEFAULT '20',
  `userImgUrl` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of FrameUserExt
-- ----------------------------
INSERT INTO `FrameUserExt` VALUES ('b1cc6cf3-12b0-4a72-9ed4-4ca786f156ba', '1', 'bootstrap', null, '30', '/topteam/UploadFiles/UserImg/arrow_left.png');
INSERT INTO `FrameUserExt` VALUES ('407c0d59-c7a3-4dba-a6a9-f8cc0aa1a74f', '12d968fc-7cbc-44e9-af9b-86a2ad29e959', null, null, '20', null);

-- ----------------------------
-- Table structure for `UserDeptRelation`
-- ----------------------------
DROP TABLE IF EXISTS `UserDeptRelation`;
CREATE TABLE `UserDeptRelation` (
  `id` varchar(50) NOT NULL,
  `userid` varchar(50) DEFAULT NULL,
  `deptid` varchar(50) DEFAULT NULL,
  `dutyid` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `isMainOu` tinyint(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of UserDeptRelation
-- ----------------------------
INSERT INTO `UserDeptRelation` VALUES ('ec4a9c97-ee50-4054-a2c5-851b665a773c', '22', 'c29466b3-974e-4382-af7d-78c857fcb55f', 'undefined', '0', '1');

-- ----------------------------
-- Table structure for `UserRoleRelation`
-- ----------------------------
DROP TABLE IF EXISTS `UserRoleRelation`;
CREATE TABLE `UserRoleRelation` (
  `id` varchar(50) NOT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `roleId` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of UserRoleRelation
-- ----------------------------
INSERT INTO `UserRoleRelation` VALUES ('380e98db-719f-4baf-b163-39ca17953f55', '1', '22391b9b-e7fb-43bc-9702-107db427c995', '0');
INSERT INTO `UserRoleRelation` VALUES ('9aa14494-35d5-4d30-a67f-0fd4c8d27936', '1', '2ffccd5e-39dd-47f8-a3f4-d3a46df8572f', '0');
INSERT INTO `UserRoleRelation` VALUES ('a9a1498e-1924-4aff-a325-f690adb24bcf', '8', '22391b9b-e7fb-43bc-9702-107db427c995', '0');
INSERT INTO `UserRoleRelation` VALUES ('d0ddcfc4-afd0-44a5-b6de-1c4be0d51929', '8', '2ffccd5e-39dd-47f8-a3f4-d3a46df8572f', '0');
INSERT INTO `UserRoleRelation` VALUES ('26ce9f98-95e8-434c-8df8-1903f9cdc703', '20', '2cd9f6c9-48bf-4d5f-bf81-8df02802ef38', '0');
INSERT INTO `UserRoleRelation` VALUES ('b923ced0-a95f-4567-816f-7ad01aeaa251', '20', '2ffccd5e-39dd-47f8-a3f4-d3a46df8572f', '0');
INSERT INTO `UserRoleRelation` VALUES ('bc02cf67-f070-42ec-8559-cd74834df5b1', '20', 'abc11286-5961-4be2-b83a-4f4b9fdd71b9', '0');
INSERT INTO `UserRoleRelation` VALUES ('fbccf957-2468-45a8-a6a8-dd99513381e2', '22', '5318ef25-82b3-4071-8c68-7e2494b93db2', '0');
INSERT INTO `UserRoleRelation` VALUES ('75acfe1e-081e-474f-b408-29b43ebb3dc3', '22', '2cd9f6c9-48bf-4d5f-bf81-8df02802ef38', '0');
INSERT INTO `UserRoleRelation` VALUES ('caf62477-b12b-4e0f-90d4-b64dd813a51e', '22', '2ffccd5e-39dd-47f8-a3f4-d3a46df8572f', '0');
INSERT INTO `UserRoleRelation` VALUES ('4c3c1838-1523-4d23-bee2-83104c39cdb6', '22', 'e3fdd833-6de1-4dde-95d7-d173a8940c7a', '0');
INSERT INTO `UserRoleRelation` VALUES ('52eff87d-71b1-4a40-8b23-9551ebc474f3', '22', 'abc11286-5961-4be2-b83a-4f4b9fdd71b9', '0');
INSERT INTO `UserRoleRelation` VALUES ('51e413c4-1e10-4652-bda2-3b029078ac68', 'addaab67-96ef-44b3-b040-81233742b774', '5318ef25-82b3-4071-8c68-7e2494b93db2', '0');
INSERT INTO `UserRoleRelation` VALUES ('439ec359-368c-4940-812c-3d5069c83515', '12d968fc-7cbc-44e9-af9b-86a2ad29e959', '5318ef25-82b3-4071-8c68-7e2494b93db2', '0');


package com.topteam.frame.controllor;

import java.util.ArrayList;
import java.util.List;

import com.topteam.component.freemarker.SelectItem;
import com.topteam.component.freemarker.TopButton;
import com.topteam.component.freemarker.TopSelect;
import com.topteam.core.Path;

@Path("/demo")
public class TestController extends BaseController{

	
	public void testFck(){
		render("/frame/demo/demo-fck.html");
	}
	
	public void fckSave(){
		String content = getPara("content");
		
		System.out.println(content);
		setAttr("content", content);
		redirect("/demo/testFck");
	}
	
	public void msg(){
		render("/frame/demo/demo-message.html");
	}
	
	public void calendar(){
		render("/frame/mycalendar.html");
	}
	
	public void button(){
		
		setAttr("selectValue", "bb");
		render("/frame/demo/demo-button.html");
	}
	
	public void save(){
		String test = getPara("test");
		System.out.println(test);
		
		String label = getPara("label");
		System.out.println(label);
		renderText("测试");
	}
	
	public void selects(){
		List<SelectItem> list = new ArrayList<SelectItem>();
		for(int i=1; i<=5 ; i++){
			SelectItem item = new SelectItem("label"+i,"value"+i);
			list.add(item);
		}
		renderJson(list);
	}
}

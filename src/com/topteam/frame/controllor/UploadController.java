package com.topteam.frame.controllor;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.jfinal.kit.JsonKit;
import com.jfinal.upload.UploadFile;
import com.topteam.component.freemarker.TopUpload;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameUploadFile;
import com.topteam.frame.entity.FrameUserExt;
import com.topteam.utility.DateUtil;
import com.topteam.utility.StringUtil;


@Path("/upload")
public class UploadController extends BaseController{

	
	public void index(){
		String targetId = getPara("targetId");
		String tag = getPara("tag");
		if(StringUtil.isNotNull(targetId))
			setAttr("targetId", targetId);
		if(StringUtil.isNotNull(tag))
			setAttr("tag", tag);
		render("/frame/UpLoad.html");
	}
	
	public void up(){
		String mHttpUrl=getRequest().getRealPath("/")+"\\UploadFiles\\Files\\";
		UploadFile file = getFile("Filedata", mHttpUrl);
		File f = file.getFile();
		String targetId = getPara("targetId");
		String tag = getPara("tag");
		FrameUploadFile uploadFile = new FrameUploadFile();
		uploadFile.set("id", UUID.randomUUID().toString());
		uploadFile.set("filename", file.getFileName());
		uploadFile.set("filepath", "/UploadFiles/");
		uploadFile.set("filetime", f.lastModified());
		uploadFile.set("filesize", f.length());
		uploadFile.set("tag", tag);
		uploadFile.set("targetid", targetId);
		uploadFile.save();
		renderJson(JsonKit.toJson(uploadFile));
	}
	
	//TODO 多附件上传
	public void ups(){
		String mHttpUrl=getRequest().getRealPath("/")+"\\UploadFiles\\Files\\";
		List<UploadFile> files =  getFiles(mHttpUrl);
		String targetId = getPara("targetId");
		String tag = getPara("tag");
		List<FrameUploadFile> list = new  ArrayList<FrameUploadFile>();
		for (UploadFile file : files) {
			File f = file.getFile();
			FrameUploadFile uploadFile = new FrameUploadFile();
			uploadFile.set("id", UUID.randomUUID().toString());
			uploadFile.set("filename", file.getFileName());
			uploadFile.set("filepath", "/UploadFiles/");
			uploadFile.set("filetime", f.lastModified());
			uploadFile.set("filesize", f.length());
			uploadFile.set("tag", tag);
			uploadFile.set("targetid", targetId);
			uploadFile.save();
			list.add(uploadFile);
		}
		renderJson(JsonKit.toJson(list));
	}
	
	public void fckUp(){
		String date = DateUtil.convertDate2String(new Date(), "yyyy-MM-dd");
		String mHttpUrl=getRequest().getRealPath("/")+"\\UploadFiles\\Editor\\"+date+"\\";
		UploadFile file = getFile("imgFile", mHttpUrl);
		if(file!=null){
			renderJson("{\"error\" : 0,\"url\":\""+getRequest().getContextPath()+"/UploadFiles/Editor/"+date+"/"+file.getFileName()+"\"}");
		}else{
			renderJson("{\"error\" : 1,\"message\" : \"上传文件失败\"}");
		}
	}
	
	public void userImgUp(){
		String mHttpUrl=getRequest().getRealPath("/")+"\\UploadFiles\\UserImg\\";
		UploadFile file = getFile("Filedata", mHttpUrl);
		String targetId = getPara("targetId");
		FrameUserExt userExt = FrameUserExt.dao.findById(targetId);
		String url = getRequest().getContextPath()+"/UploadFiles/UserImg/"+file.getFileName();
		userExt.set("userImgUrl", url);
		userExt.update();
		renderJson(url);
	}
	
	public void delete(){
		String id = getPara("id");
		if(StringUtil.isNotNull(id)){
			FrameUploadFile.dao.deleteById(id);
		}
		renderText("success");
	}
}

package com.topteam.frame.controllor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.jfinal.plugin.activerecord.Page;
import com.topteam.component.easyui.EasyTreeModel;
import com.topteam.component.easyui.EasyTreeNode;
import com.topteam.component.easyui.Paginate;
import com.topteam.core.Path;
import com.topteam.frame.entity.FrameDept;
import com.topteam.security.Security;


@Path("/framedept")
public class FrameDeptController extends BaseController{

	public void list(){
		render("/frame/FrameDeptList.html");
	}
	
	@Security(depend="/list")
	public void save(){
		FrameDept d = getModel(FrameDept.class,"framedept");
		d.set("id", UUID.randomUUID().toString());
		d.set("deptCode", FrameDept.dao.getNextCode(d.getStr("parentId")));
		d.save();
		renderText("保存成功","text/html");
	}
	
	@Security(depend="/list")
	public void update(){
		FrameDept d = getModel(FrameDept.class,"framedept");
		
        String oldParentId = getPara("parentId");
		
		// 判断父节点是否改变，是否需要重新生成code
		boolean flag = false;
		if(oldParentId == null){
			if(d.getStr("parentId")!=null)
				flag = true;
		}else{
			if(d.getStr("parentId")==null || !oldParentId.equals(d.getStr("parentId")))
				flag = true;
		}
		if(flag){
			String newCode = FrameDept.dao.getNextCode(d.getStr("parentId"));
			d.set("deptCode", newCode);
			FrameDept.dao.updateSubCode(d,newCode);
			
		}else{
			d.set("deptCode", d.getStr("deptCode"));
		}
		
		d.update();
		renderText("修改成功","text/html");
	}
	
	@Security(depend="/list")
	public void edit(){
		FrameDept d = FrameDept.dao.findById(getPara());
		setAttr("framedept", d);
		renderFreeMarker("/frame/FrameDeptEditPage.html");
	}
	
	@Security(depend="/list")
	public void add(){
		setAttr("parentId", getPara("parentId"));
		setAttr("framedept", new FrameDept());
		renderFreeMarker("/frame/FrameDeptAddPage.html");
	}
	
	@Security(depend="/list")
	public void delete(){
		String ids = getPara("ids");
		FrameDept.dao.deleteIds(ids);
		renderJson();
	}
	
	@Security(depend="/list")
	public void table(){
		String code = getPara("code");
		int page = getParaToInt("page");
		int size = getParaToInt("rows");
		Page<FrameDept> p = null;
		if(code==null){
			p= FrameDept.dao.getDeptPage(page, size);
		}else{
			p = FrameDept.dao.getDeptsByCode(code,page, size);
		}
		
		Paginate paginate = Paginate.build(p);
		renderJson(paginate);
	}
	
	public void leftTree(){
		EasyTreeModel<FrameDept> model = new EasyTreeModel<FrameDept>(this) {
			private static final long serialVersionUID = -5310992744165040365L;
			
			@Override
			public EasyTreeNode model2Node(FrameDept t) {
				EasyTreeNode treeNode = new EasyTreeNode();
				treeNode.setId(t.getStr("id"));
				treeNode.setText(t.getStr("deptName"));
				Map<String, String> map = new HashMap<String, String>();
				map.put("deptCode", t.getStr("deptCode"));
				map.put("isreal", t.getInt("isreal").toString());
				treeNode.setAttributes(map);
				treeNode.setHasChild(t.getLong("has")>0L);
				return treeNode;
			}
			
			@Override
			public List<FrameDept> fechDate(String id) {
				List<FrameDept> list = new ArrayList<FrameDept>();
				if(id == null){
					String sql = "select t.*, case when (SELECT count(1) from FrameDept t2 where t2.parentId=t.id)>0 then 1 ELSE 0 END as has  from FrameDept t where t.parentId is null order by t.order desc";
					list = FrameDept.dao.find(sql);
				}
				else{
					String sql = "select t.*, case when (SELECT count(1) from FrameDept t2 where t2.parentId=t.id)>0 then 1 ELSE 0 END as has  from FrameDept t where t.parentId=? order by t.order desc";
					list = FrameDept.dao.find(sql,id);
				}
				return list;
			}
		};
		
		renderJson(model.toJson());
	}
}

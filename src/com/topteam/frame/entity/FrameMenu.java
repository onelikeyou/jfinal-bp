package com.topteam.frame.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBean;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.topteam.utility.StringUtil;

@ModelBean("FrameMenu")
public class FrameMenu extends Model<FrameMenu> {

	private static final long serialVersionUID = -3743632753274581081L;
	public static final FrameMenu dao = new FrameMenu();
	
	public List<FrameMenu> getRootMenu(){
		String sql = "select t.* from FrameMenu t where t.parentId is null or t.parentId='' order by t.order desc";
		return find(sql);
	}
	
	public List<FrameMenu> getAllMenu(){
		String sql = "select t.* from FrameMenu t order by t.order desc";
		return find(sql);
	}
	
	public Page<FrameMenu> getMenuPage(int page, int size){
		return paginate(page, size, "select t.*", " from FrameMenu t order by t.order desc");
	}
	
	public List<FrameMenu> getMenusByParentId(String id,String type){
		String sql = "select * from FrameMenu where 1=1";
		if(StringUtil.isNotNull(id))
			sql += " and parentId='"+id+"'";
		else
			sql += " and parentId is null";
		if(StringUtil.isNotNull(id))
			sql += " and type = '"+type+"'";
		return find(sql);
	}
	
	public Page<FrameMenu> getMenusByCode(String code,int page, int size){
		return paginate(page, size, "select t.*", " from FrameMenu t where t.menuCode like '"+code+"%' order by  t.order desc");
	}
	
	public String getNextCode(String parentId){
		String sql =null;
		String parentCode = "";
		if(StringUtil.isNull(parentId)){
			sql = "select max(menuCode) from FrameMenu t where LENGTH(t.menuCode)=4 ";
		}else{
			FrameMenu parent = findById(parentId);
			parentCode = parent.getStr("menuCode");
			if(StringUtil.isNull(parentCode)){
				sql = "select max(menuCode) from FrameMenu t where LENGTH(t.menuCode)=4 ";
			}else{
				int length = parentCode.length();
				sql = "select max(menuCode) from FrameMenu t where t.parentId = '"+parentId+"' AND LENGTH(t.menuCode)="+(length+4)+" and substring(t.menuCode,1,"+length+")='"+parentCode+"'";
			}
		}
		String code = Db.queryStr(sql);
		if(code == null){
			return parentCode+"0001";
		}else{
			return StringUtil.nextCode(code);
		}
	}

	public void deleteIds(String ids) {
		String[] id = ids.split(";");
		for (int i = 0; i < id.length; i++) {
			deleteById(id[i]);
		}
	}

	public void updateSubCode(FrameMenu m, String newCode) {
		String code = m.getStr("menuCode") == null?"":m.getStr("menuCode");
		
		List<FrameMenu> list = find("select * from FrameMenu where menuCode like '" + code + "%'");
		for(int i =0 ; i< list.size();i++){
			FrameMenu fm  = list.get(i);
			fm.set("menuCode", newCode+fm.getStr("menuCode").substring(code.length()));
			fm.update();
		}
	}

	public Page<Record> getMenuByCode(String code, int page, int size) {
		String sql = " from FrameMenu t where 1=1 ";
		if(StringUtil.isNotNull(code))
			sql +=" and t.menuCode like '"+code+"%'";
		sql += " order by t.order desc";
		return Db.paginate(page, size, "select * ", sql);
	}
	
	public List<FrameMenu> queryAuth(String type, String userId){
		String menuSql = "select m.* from FrameAuth a,FrameMenu m WHERE a.resource = m.id AND m.type=? AND a.openAll='1' "
				+"UNION " 
				+"(SELECT m.* FROM FrameUser u LEFT JOIN UserRoleRelation ur ON u.id=ur.userId LEFT JOIN FrameAuth a ON ur.roleId = a.grantTo LEFT JOIN FrameMenu m ON a.resource = m.id WHERE m.type=? AND u.id=?) "
				+"UNION "
				+"(SELECT m.* FROM FrameUser u LEFT JOIN UserDeptRelation ud ON u.id = ud.userid LEFT JOIN FrameAuth a ON (ud.deptid=a.grantTo OR ud.dutyid=a.grantTo) LEFT JOIN FrameMenu m ON a.resource=m.id WHERE m.type=? AND u.id=?) " 
				+"UNION "
				+"(SELECT m.* FROM FrameUser u LEFT JOIN FrameAuth a ON u.id=a.grantTo LEFT JOIN FrameMenu m ON a.resource=m.id where m.type=? and u.id=?) ";
		return FrameMenu.dao.find(menuSql, type,type,userId,type,userId,type,userId);
	}
}

package com.topteam.frame.vo;



import com.jfinal.kit.JsonKit;
import com.topteam.component.easyui.Paginate;
import com.topteam.frame.entity.FrameDept;
import com.topteam.frame.entity.FrameUser;
import com.topteam.utility.VOUtil;

public class FrameUserVO {

	private String id;
	
	@Porperty(name="登录名", group="基本信息")
	private String name;
	
	@Porperty(name="显示名", group="基本信息")
	private String displayname;
	
	@Porperty(name="性别", group="基本信息")
	private String sex;
	
	@Porperty(name="邮箱", group="基本信息")
	private String email;
	
	@Porperty(name="电话", group="基本信息")
	private String tel;
	
	@Porperty(name="部门", group="部门信息")
	private String ouname;
	
	
	public FrameUserVO() {
		super();
	}

	public FrameUserVO(FrameUser user, FrameDept dept){
		this.id = user.get("id");
		this.name=user.get("username"); 
		this.displayname=user.get("displayname");
		this.sex=user.get("sex");
		this.email=user.get("mail");
		this.tel=user.get("tel");
		this.ouname=dept.get("deptname");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getOuname() {
		return ouname;
	}

	public void setOuname(String ouname) {
		this.ouname = ouname;
	}
	
	
	public static void main(String[] args) {
		FrameUserVO o = new FrameUserVO();
		o.setId("123");
		
		o.setSex("男");
		o.setDisplayname("eeee");
		o.setName("qqq");
		o.setEmail("eeeeeeeeeeeeeee");
		o.setOuname("fffff");
		o.setTel("12321");
		System.out.println(JsonKit.toJson(Paginate.build(VOUtil.porperty2MapList(o))));
	}
}

package com.topteam.component.freemarker;

import java.awt.event.FocusAdapter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.topteam.frame.controllor.BaseController;
import com.topteam.utility.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 表格控件 Column 组
 * 
 * @author Jiang Feng
 * 
 */
public class TopColumns extends TopTemplateModel {

	public static final String TAG = "columns";

	private BaseController controller;

	private boolean frozen;

	private boolean group;

	public TopColumns(BaseController controller) {
		this.controller = controller;
	}

	private void initPara(Map para, Environment env) throws TemplateException {
		frozen = getBoolPara("frozen", para, env,frozen);
		group = getBoolPara("group", para, env,group);
	}

	@Override
	public void execute(Environment env, Map para, TemplateModel[] arg2,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		initPara(para, env);
		Writer w = env.getOut();
		if (!group) {
			w.write("	<thead ");
			if (frozen)
				w.write("frozen=\"" + frozen + "\"");
			w.write(">\r\n");
		}
		w.write("		<tr>\r\n");
		body.render(w);
		w.write("		</tr>\r\n");
		if (!group)
			w.write("	</thead>\r\n");
		// writeScript(w);
	}

	public void writeScript(Writer w) throws IOException {
		w.write("<script type=\"text/javascript\">");

		w.write("</script>");
	}

}

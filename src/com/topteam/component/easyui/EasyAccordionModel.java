package com.topteam.component.easyui;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;


import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

public class EasyAccordionModel implements TemplateDirectiveModel{
	
	public static final String TAG_NAME= "e_accordion";

	@Override
	public void execute(Environment env, Map para, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		Writer out = env.getOut();
		
		List<EasyAccordion> list = (List<EasyAccordion>) WrappingTemplateModel.getDefaultObjectWrapper().wrap(para.get("list").getClass());
		System.out.println(list.size());
		
		out.write("<div id=\"aa\" class=\"easyui-accordion\" data-options=\"fit:true\" >");
		out.write("   <div title=\"www\" >www");
		out.write("   </div >");
		out.write("</div>");
		body.render(env.getOut());
	}

}

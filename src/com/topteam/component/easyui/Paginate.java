package com.topteam.component.easyui;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;

public class Paginate {

	private int total;
	
	private List<?> rows;
	
	public static Paginate build(List<?> rows){
		Paginate p = new Paginate();
		p.setRows(rows);
		p.setTotal(rows.size());
		return p;
	}
	
	public static Paginate build(List<?> rows, int total){
		Paginate p = new Paginate();
		p.setRows(rows);
		p.setTotal(total);
		return p;
	}
	
	public static Paginate build(Page<?> page){
		Paginate p = new Paginate();
		p.setRows(page.getList());
		p.setTotal(page.getTotalRow());
		return p;
	}
	
	public static Paginate build(List<?> rows, int total,int page, int size){
		Paginate p = new Paginate();
		p.setRows(rows);
		p.setTotal(total);
		return p;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

}

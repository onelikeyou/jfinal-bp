package com.topteam.component.easyui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;

public abstract class EasyTreeModel<T> implements Serializable{
	
	private static final long serialVersionUID = 45941477530969284L;

	private List<EasyTreeNode> treeNode = new ArrayList<EasyTreeNode>();
	
	private Controller controller;
	
	private String rootName;
	private String rootId;
	private T root;
	
	private boolean expandAll;
	
	public EasyTreeModel(Controller controller){
		this.controller = controller;
	}

	public abstract List<T> fechDate(String id);
	
	public abstract EasyTreeNode model2Node(T t);
	
	public boolean isExpandAll() {
		return expandAll;
	}

	public void setExpandAll(boolean expandAll) {
		this.expandAll = expandAll;
	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	public String getRootId() {
		return rootId;
	}

	public void setRootId(String rootId) {
		this.rootId = rootId;
	}

	public T getRoot() {
		return root;
	}

	public void setRoot(T root) {
		this.root = root;
	}

	public String toJson() {
		String id = controller.getPara("id");
		if(id==null){
			id= controller.getPara("rootId");;
		}
		List<T> list = fechDate(id);
		for(T t : list){
			EasyTreeNode node = model2Node(t);
			treeNode.add(node);
			
		}
		return JsonKit.toJson(treeNode);
	}
	
}

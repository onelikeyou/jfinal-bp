package com.topteam.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.druid.DruidPlugin;
import com.topteam.application.TopApplicationContext;
import com.topteam.component.freemarker.FreeMarkerInterceptor;
import com.topteam.frame.controllor.FrameRoutes;
import com.topteam.frame.controllor.IndexController;
import com.topteam.frame.controllor.TestController;
import com.topteam.security.SecurityInterceptor;

public class TopJFinalConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("topteam.properties");
		me.setDevMode(getPropertyToBoolean("devMode", false));
		me.setError404View("/404.html");
		me.setError500View("/500.html");
		me.setError403View("/403.html");
	}

	@Override
	public void configRoute(Routes me) {
		me.add(IndexController.class);
		// 控件核心控制器
		me.add(new FrameRoutes());

		me.add(TestController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		
//		QuartzPlugin quartzPlugin = new QuartzPlugin("quartzjob.properties");
//		me.add(quartzPlugin);
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password").trim());
		me.add(druidPlugin);
//		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"),
//				getProperty("user"), getProperty("password").trim());
//		me.add(c3p0Plugin);

		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		//arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		arp.addPackage("com.topteam.frame.entity");
		me.add(arp);
		
		
		 // 添加自动绑定model与表插件
//        AutoTableBindPlugin autoTableBindPlugin = new AutoTableBindPlugin(c3p0Plugin, SimpleNameStyles.LOWER);
//        autoTableBindPlugin.setShowSql(true);
//        autoTableBindPlugin.setContainerFactory(new CaseInsensitiveContainerFactory());
//        me.add(autoTableBindPlugin);
      //  me.add(new SqlInXmlPlugin());
        
	}

	@Override
	public void configInterceptor(Interceptors me) {
		//me.add(new RootInterceptor());
		me.add(new SecurityInterceptor());
		me.add(new Tx());  // 全局事务配置
		me.add(new FreeMarkerInterceptor());
	}

	/**
	 * 服务器启动时，初始化系统相关参数
	 */
	@Override
	public void configHandler(Handlers me) {
		TopApplicationContext.getInstance().initialize();
	}

}

﻿var StringUtil = {};

StringUtil.array2Str = function(rows, attr, split) {
	var attrs = new Array();
	for ( var i = 0; i < rows.length; i++) {
		var a = eval("rows[" + i + "]." + attr);
		attrs[attrs.length] = a;
	}
	if (typeof (split) == "undefined") {
		return attrs.join(",");
	} else {
		return attrs.join(split);
	}
};

var DateUtil = {};

DateUtil.long2Date = function(val, formatter) {
	var d = new Date(val);
	return DateUtil.date2String(d, formatter);
};

DateUtil.date2String = function(date, format) {
	if (!Util.isDate(date) || Util.isNull(format)) {
		return "";
	}
	var o = {
		"M+" : date.getMonth() + 1, // month
		"d+" : date.getDate(), // day
		"H+" : date.getHours(), // hour
		"m+" : date.getMinutes(), // minute
		"s+" : date.getSeconds(), // second
		"q+" : Math.floor((date.getMonth() + 3) / 3), // quarter
		"S" : date.getMilliseconds()
	// millisecond
	};

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (date.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}

	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
};

DateUtil.string2Date = function(str, format) {
	if(Util.isNull(str) || Util.isNull(format)) {
		return null;
	}
	var compare = {
		'y+' : 'y',
		'M+' : 'M',
		'd+' : 'd',
		'H+' : 'h',
		'm+' : 'm',
		's+' : 's'
	};
	var result = {
		'y' : '',
		'M' : '',
		'd' : '',
		'H' : '00',
		'm' : '00',
		's' : '00'
	};
	var tmp = format;
	for(var k in compare) {
		if(new RegExp('(' + k + ')').test(format)) {
			result[compare[k]] = str.substring(tmp.indexOf(RegExp.$1), tmp.indexOf(RegExp.$1) + RegExp.$1.length);
		}
	}
	return new Date(result['y'], result['M'] - 1, result['d'], result['H'], result['m'], result['s']);
};

var Util = {};

Util.isDate = function(o) {
	return Object.prototype.toString.call(o) === "[object Date]";
};

Util.isNull = function(o) {
	return (0 == null || typeof (o) == 'undefined');
};

var Alert = {};

Alert.info = function(msg, title, event) {
	var t = title ? title : '系统消息';
	if (event == 'refresh') {
		$.messager.alert(t, msg, 'info', function() {
			window.location = window.location.href.replace("#", "");
		});
	} else if (event == 'close') {
		$.messager.alert(t, msg, 'info', function() {
			window.close();
		});
	} else if (typeof (event) == 'function') {
		$.messager.alert(t, msg, 'info', event);
	} else {
		$.messager.alert(t, msg, 'info');
	}
};

Alert.error = function(msg, title, event) {
	var t = title ? title : '系统消息';
	if (event == 'refresh') {
		$.messager.alert(t, msg, 'error', function() {
			window.location = window.location.href.replace("#", "");
		});
	} else if (event == 'close') {
		$.messager.alert(t, msg, 'error', function() {
			window.close();
		});
	} else if (typeof (event) == 'function') {
		$.messager.alert(t, msg, 'error', event);
	} else {
		$.messager.alert(t, msg, 'error');
	}
};

Alert.question = function(msg, title, event) {
	var t = title ? title : '系统消息';
	if (event == 'refresh') {
		$.messager.alert(t, msg, 'question', function() {
			window.location = window.location.href.replace("#", "");
		});
	} else if (event == 'close') {
		$.messager.alert(t, msg, 'question', function() {
			window.close();
		});
	} else if (typeof (event) == 'function') {
		$.messager.alert(t, msg, 'question', event);
	} else {
		$.messager.alert(t, msg, 'question');
	}
};

Alert.warning = function(msg, title, event) {
	var t = title ? title : '系统消息';
	if (event == 'refresh') {
		$.messager.alert(t, msg, 'warning', function() {
			window.location = window.location.href.replace("#", "");
		});
	} else if (event == 'close') {
		$.messager.alert(t, msg, 'warning', function() {
			window.close();
		});
	} else if (typeof (event) == 'warning') {
		$.messager.alert(t, msg, 'question', event);
	} else {
		$.messager.alert(t, msg, 'warning');
	}
};

